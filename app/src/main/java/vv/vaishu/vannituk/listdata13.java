package vv.vaishu.vannituk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import vv.vaishu.vannituk.Area.Area_12;
import vv.vaishu.vannituk.Area.Area_13;

public class listdata13 extends AppCompatActivity {
    private static final int REQUEST_CALL = 1;
    TextView listdata;
    ImageView imageView;
    private  TextView phoneNumber;
    ImageButton backbtn13;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdata13);
        listdata = findViewById(R.id.listdata10);
        imageView = findViewById(R.id.imageView10);
        phoneNumber = findViewById(R.id.edit_text_numbers10);
        ImageView imageCall = findViewById(R.id.image_calls10);
        backbtn13 = findViewById(R.id.back10);

        Intent intent = getIntent();
        String receivedName10 =  intent.getStringExtra("name13");
        int receivedImage10 = intent.getIntExtra("image13",0);
        String recievedNumber10 = intent.getStringExtra("phoneNo13");
        listdata.setText(receivedName10);
        imageView.setImageResource(receivedImage10);
        phoneNumber.setText(recievedNumber10);

        //enable back Button
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCalls();
            }
        });
        backbtn13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back13();
            }
        });

    }
    private  void back13(){
        Intent intent =new Intent(listdata13.this, Area_13.class);
        startActivity(intent);
    }


    private void makePhoneCalls() {
        String number = phoneNumber.getText().toString();
        if (number.trim().length() > 0) {

            if (ContextCompat.checkSelfPermission(listdata13.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(listdata13.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        } else {
            Toast.makeText(listdata13.this, "Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
