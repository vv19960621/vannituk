package vv.vaishu.vannituk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import vv.vaishu.vannituk.Area.Area_2;
import vv.vaishu.vannituk.Area.Area_3;

public class list_data3 extends AppCompatActivity {
    private static final int REQUEST_CALL = 1;
    TextView listdata;
    ImageView imageView;
    private  TextView phoneNumber;
    ImageButton backbtn3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdata3);
        listdata = findViewById(R.id.listdata3);
        imageView = findViewById(R.id.imageView3);
        phoneNumber = findViewById(R.id.edit_text_numbers3);
        ImageView imageCall = findViewById(R.id.image_calls3);
        backbtn3 = findViewById(R.id.back3);

        Intent intent = getIntent();
        String receivedName3 =  intent.getStringExtra("name3");
        int receivedImage3 = intent.getIntExtra("image3",0);
        String recievedNumber3 = intent.getStringExtra("phoneNo3");
        listdata.setText(receivedName3);
        imageView.setImageResource(receivedImage3);
        phoneNumber.setText(recievedNumber3);

        //enable back Button
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCalls();
            }
        });
        backbtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back3();
            }
        });

    }

    private  void back3(){
        Intent intent =new Intent(list_data3.this, Area_3.class);
        startActivity(intent);
    }


    private void makePhoneCalls() {
        String number = phoneNumber.getText().toString();
        if (number.trim().length() > 0) {

            if (ContextCompat.checkSelfPermission(list_data3.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(list_data3.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        } else {
            Toast.makeText(list_data3.this, "Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
