package vv.vaishu.vannituk;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;

import vv.vaishu.vannituk.Area.Area_10;
import vv.vaishu.vannituk.Area.Area_11;
import vv.vaishu.vannituk.Area.Area_12;
import vv.vaishu.vannituk.Area.Area_13;
import vv.vaishu.vannituk.Area.Area_14;
import vv.vaishu.vannituk.Area.Area_15;
import vv.vaishu.vannituk.Area.Area_2;
import vv.vaishu.vannituk.Area.Area_3;
import vv.vaishu.vannituk.Area.Area_4;
import vv.vaishu.vannituk.Area.Area_5;
import vv.vaishu.vannituk.Area.Area_6;
import vv.vaishu.vannituk.Area.Area_7;
import vv.vaishu.vannituk.Area.Area_8;
import vv.vaishu.vannituk.Area.Area_9;

public class AreaListPageMain extends AppCompatActivity {

    Button chapter1btn;
    Button chapter2btn;
    Button chapter3btn;
    Button chapter4btn;
    Button chapter5btn;
    Button chapter6btn;
    Button chapter7btn;
    Button chapter8btn;
    Button chapter9btn;
    Button chapter10btn;
    Button chapter11btn;
    Button chapter12btn;
    Button chapter13btn;
    Button chapter14btn;
    Button chapter15btn;
    ImageButton homebutton;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_list_page_main);

        chapter1btn = (Button) findViewById(R.id.chapterbtn1);
        chapter2btn =(Button) findViewById(R.id.chapterbtn2);
        chapter3btn =(Button) findViewById(R.id.chapterbtn3);
        chapter4btn = (Button) findViewById(R.id.chapterbtn4);
        chapter5btn =(Button) findViewById(R.id.chapterbtn5);
        chapter6btn =(Button) findViewById(R.id.chapterbtn6);

        chapter7btn = (Button) findViewById(R.id.chapterbtn7);
        chapter8btn =(Button) findViewById(R.id.chapterbtn8);
        chapter9btn =(Button) findViewById(R.id.chapterbtn9);

        chapter10btn = (Button) findViewById(R.id.chapterbtn10);
        chapter11btn =(Button) findViewById(R.id.chapterbtn11);
        chapter12btn =(Button) findViewById(R.id.chapterbtn12);

        chapter13btn = (Button) findViewById(R.id.chapterbtn13);
        chapter14btn =(Button) findViewById(R.id.chapterbtn14);
        chapter15btn =(Button) findViewById(R.id.chapterbtn15);
        homebutton =(ImageButton) findViewById(R.id.homebutton);

        final Animation animAlpha2 = AnimationUtils.loadAnimation(this, R.anim.fade);


        chapter1btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_list.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });

        chapter2btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_2.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });

        chapter3btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_3.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });
        chapter4btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_4.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });

        chapter5btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_5.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });

        chapter6btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_6.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });
        chapter7btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_7.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });

        chapter8btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_8.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });

        chapter9btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_9.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });
        chapter10btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_10.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });

        chapter11btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_11.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });

        chapter12btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_12.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });
        chapter13btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_13.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });

        chapter14btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_14.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });

        chapter15btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),Area_15.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });
        homebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),MainActivity.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
        });


    }



    private void home_page(){
        Intent intent =new Intent(AreaListPageMain.this, Area_list.class);
        startActivity(intent);
    }

    private void chapter2_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_2.class);
        startActivity(intent);
    }

    private void chapter3_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_3.class);
        startActivity(intent);
    }
    private void chapter4_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_4.class);
        startActivity(intent);
    }

    private void chapter5_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_5.class);
        startActivity(intent);
    }
    private void chapter6_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_6.class);
        startActivity(intent);
    }

    private void chapter7_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_7.class);
        startActivity(intent);
    }
    private void chapter8_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_8.class);
        startActivity(intent);
    }

    private void chapter9_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_9.class);
        startActivity(intent);
    }
    private void chapter10_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_10.class);
        startActivity(intent);
    }

    private void chapter11_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_11.class);
        startActivity(intent);
    }
    private void chapter12_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_12.class);
        startActivity(intent);
    }

    private void chapter13_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_13.class);
        startActivity(intent);
    }
    private void chapter14_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_14.class);
        startActivity(intent);
    }

    private void chapter15_eng(){
        Intent intent =new Intent(AreaListPageMain.this, Area_15.class);
        startActivity(intent);
    }

    private void mainActivity(){
        Intent intent =new Intent(AreaListPageMain.this,MainActivity.class);
        startActivity(intent);
    }


    public void clickexit(View view) {

        moveTaskToBack(true);
        Process.killProcess(Process.myPid());
        System.exit(1);
    }
//    public  void taptoFade(){
//        Button Button = (Button)findViewById(R.id.chapterbtn1);
//        Animation animation = AnimationUtils.loadAnimation(this,R.anim.slide);
//        Button.startAnimation(animation);
//    }

}
