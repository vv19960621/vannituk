package vv.vaishu.vannituk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class listdata extends AppCompatActivity {
    private static final int REQUEST_CALL = 1;
    TextView listdata;
    ImageView imageView;
    ImageButton backbtn;
    private  TextView phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdata);
       // phoneNumber = findViewById(R.id.edit_text_numbers)
        listdata = findViewById(R.id.listdata);
        imageView = findViewById(R.id.imageView);
        phoneNumber = findViewById(R.id.edit_text_numbers);
        backbtn = findViewById(R.id.back);
        ImageView imageCall = findViewById(R.id.image_calls);

        Intent intent = getIntent();
        String receivedName =  intent.getStringExtra("name");
        int receivedImage = intent.getIntExtra("image",0);
        String recievedNumber = intent.getStringExtra("phoneNo");
        listdata.setText(receivedName);
        imageView.setImageResource(receivedImage);
        phoneNumber.setText(recievedNumber);

        //enable back Button
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

        imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCall();
            }
        });

    }

    private void back(){
        Intent intent =new Intent(listdata.this, Area_list.class);
        startActivity(intent);
    }

    private void makePhoneCall() {
        String number = phoneNumber.getText().toString();
        if (number.trim().length() > 0) {

            if (ContextCompat.checkSelfPermission(listdata.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(listdata.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        } else {
            Toast.makeText(listdata.this, "Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
