package vv.vaishu.vannituk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import vv.vaishu.vannituk.Area.Area_4;
import vv.vaishu.vannituk.Area.Area_5;

public class listdata5 extends AppCompatActivity {

    private static final int REQUEST_CALL = 1;
    TextView listdata;
    ImageView imageView;
    private  TextView phoneNumber;
    ImageButton backbtn5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdata5);
        listdata = findViewById(R.id.listdata5);
        imageView = findViewById(R.id.imageView5);
        phoneNumber = findViewById(R.id.edit_text_numbers5);
        ImageView imageCall = findViewById(R.id.image_calls5);
        backbtn5 = findViewById(R.id.back5);

        Intent intent = getIntent();
        String receivedName5 =  intent.getStringExtra("name5");
        int receivedImage5 = intent.getIntExtra("image5",0);
        String recievedNumber5 = intent.getStringExtra("phoneNo5");
        listdata.setText(receivedName5);
        imageView.setImageResource(receivedImage5);
        phoneNumber.setText(recievedNumber5);

        //enable back Button
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCalls();
            }
        });
        backbtn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back5();
            }
        });

    }
    private  void back5(){
        Intent intent =new Intent(listdata5.this, Area_5.class);
        startActivity(intent);
    }


    private void makePhoneCalls() {
        String number = phoneNumber.getText().toString();
        if (number.trim().length() > 0) {

            if (ContextCompat.checkSelfPermission(listdata5.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(listdata5.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        } else {
            Toast.makeText(listdata5.this, "Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
