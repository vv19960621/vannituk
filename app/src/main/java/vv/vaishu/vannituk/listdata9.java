package vv.vaishu.vannituk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import vv.vaishu.vannituk.Area.Area_8;
import vv.vaishu.vannituk.Area.Area_9;

public class listdata9 extends AppCompatActivity {

    private static final int REQUEST_CALL = 1;
    TextView listdata;
    ImageView imageView;
    private  TextView phoneNumber;
    ImageButton backbtn9;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdata9);
        listdata = findViewById(R.id.listdata9);
        imageView = findViewById(R.id.imageView9);
        phoneNumber = findViewById(R.id.edit_text_numbers9);
        ImageView imageCall = findViewById(R.id.image_calls9);
        backbtn9 = findViewById(R.id.back9);

        Intent intent = getIntent();
        String receivedName9 =  intent.getStringExtra("name9");
        int receivedImage9 = intent.getIntExtra("image9",0);
        String recievedNumber9 = intent.getStringExtra("phoneNo9");
        listdata.setText(receivedName9);
        imageView.setImageResource(receivedImage9);
        phoneNumber.setText(recievedNumber9);

        //enable back Button
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCalls();
            }
        });
        backbtn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back9();
            }
        });

    }
    private  void back9(){
        Intent intent =new Intent(listdata9.this, Area_9.class);
        startActivity(intent);
    }


    private void makePhoneCalls() {
        String number = phoneNumber.getText().toString();
        if (number.trim().length() > 0) {

            if (ContextCompat.checkSelfPermission(listdata9.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(listdata9.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        } else {
            Toast.makeText(listdata9.this, "Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
