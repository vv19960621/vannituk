package vv.vaishu.vannituk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import vv.vaishu.vannituk.Area.Area_5;
import vv.vaishu.vannituk.Area.Area_6;

public class listdata6 extends AppCompatActivity {

    private static final int REQUEST_CALL = 1;
    TextView listdata;
    ImageView imageView;
    private  TextView phoneNumber;
    ImageButton backbtn6;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdata6);
        listdata = findViewById(R.id.listdata6);
        imageView = findViewById(R.id.imageView6);
        phoneNumber = findViewById(R.id.edit_text_numbers6);
        ImageView imageCall = findViewById(R.id.image_calls6);
        backbtn6 = findViewById(R.id.back6);

        Intent intent = getIntent();
        String receivedName6 =  intent.getStringExtra("name6");
        int receivedImage6 = intent.getIntExtra("image6",0);
        String recievedNumber6 = intent.getStringExtra("phoneNo6");
        listdata.setText(receivedName6);
        imageView.setImageResource(receivedImage6);
        phoneNumber.setText(recievedNumber6);

        //enable back Button
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCalls();
            }
        });
        backbtn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back6();
            }
        });

    }
    private  void back6(){
        Intent intent =new Intent(listdata6.this, Area_6.class);
        startActivity(intent);
    }


    private void makePhoneCalls() {
        String number = phoneNumber.getText().toString();
        if (number.trim().length() > 0) {

            if (ContextCompat.checkSelfPermission(listdata6.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(listdata6.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        } else {
            Toast.makeText(listdata6.this, "Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
