package vv.vaishu.vannituk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import vv.vaishu.vannituk.Area.Area_7;
import vv.vaishu.vannituk.Area.Area_8;

public class listdata8 extends AppCompatActivity {

    private static final int REQUEST_CALL = 1;
    TextView listdata;
    ImageView imageView;
    private  TextView phoneNumber;
    ImageButton backbtn8;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdata8);
        listdata = findViewById(R.id.listdata8);
        imageView = findViewById(R.id.imageView8);
        phoneNumber = findViewById(R.id.edit_text_numbers8);
        ImageView imageCall = findViewById(R.id.image_calls8);
        backbtn8 = findViewById(R.id.back8);

        Intent intent = getIntent();
        String receivedName8 =  intent.getStringExtra("name8");
        int receivedImage8 = intent.getIntExtra("image8",0);
        String recievedNumber8 = intent.getStringExtra("phoneNo8");
        listdata.setText(receivedName8);
        imageView.setImageResource(receivedImage8);
        phoneNumber.setText(recievedNumber8);

        //enable back Button
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCalls();
            }
        });
        backbtn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back8();
            }
        });

    }
    private  void back8(){
        Intent intent =new Intent(listdata8.this, Area_8.class);
        startActivity(intent);
    }


    private void makePhoneCalls() {
        String number = phoneNumber.getText().toString();
        if (number.trim().length() > 0) {

            if (ContextCompat.checkSelfPermission(listdata8.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(listdata8.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        } else {
            Toast.makeText(listdata8.this, "Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
