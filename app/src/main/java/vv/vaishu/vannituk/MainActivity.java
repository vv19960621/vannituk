package vv.vaishu.vannituk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import javax.crypto.Mac;

public class MainActivity extends AppCompatActivity {
    private static int SPLASH_TIMEOUT = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler() . postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent homeIntent = new Intent(MainActivity.this, AreaListPageMain.class);
                startActivity(homeIntent);
                finish();
            }
        },SPLASH_TIMEOUT);

    }
}
