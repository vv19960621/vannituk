package vv.vaishu.vannituk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import vv.vaishu.vannituk.Area.Area_3;
import vv.vaishu.vannituk.Area.Area_4;

public class listdata4 extends AppCompatActivity {

    private static final int REQUEST_CALL = 1;
    TextView listdata;
    ImageView imageView;
    private  TextView phoneNumber;
    ImageButton backbtn4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdata4);
        listdata = findViewById(R.id.listdata4);
        imageView = findViewById(R.id.imageView4);
        phoneNumber = findViewById(R.id.edit_text_numbers4);
        ImageView imageCall = findViewById(R.id.image_calls4);
        backbtn4 = findViewById(R.id.back4);

        Intent intent = getIntent();
        String receivedName4 =  intent.getStringExtra("name4");
        int receivedImage4 = intent.getIntExtra("image4",0);
        String recievedNumber4 = intent.getStringExtra("phoneNo4");
        listdata.setText(receivedName4);
        imageView.setImageResource(receivedImage4);
        phoneNumber.setText(recievedNumber4);

        //enable back Button
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCalls();
            }
        });
        backbtn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back4();
            }
        });

    }
    private  void back4(){
        Intent intent =new Intent(listdata4.this, Area_4.class);
        startActivity(intent);
    }


    private void makePhoneCalls() {
        String number = phoneNumber.getText().toString();
        if (number.trim().length() > 0) {

            if (ContextCompat.checkSelfPermission(listdata4.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(listdata4.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        } else {
            Toast.makeText(listdata4.this, "Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
