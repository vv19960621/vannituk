package vv.vaishu.vannituk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

public class Area_list extends AppCompatActivity {
    ListView listView;
    private SearchView mysearchviewbar;
    ImageButton turnoff;
    ImageButton homebtn;


    String[] fruitNames = {"Rajini","Kamal","Raj","Mohan","karthik","sundhar","muththu","kumar","raju","gowtham","muruga","santhosh"};
    int[] fruitImages = {R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro};

    String[] place = {"bus_stand","hospital","hospital","oldBus_Stand","postOffice","market","hospital","Bus_stand","postOffice","market","hospital","Bus_stand"};
    String[] phoneNo = {"456","0764827382","0778405238","0767513323","456","456","456","456","456","456","456","456"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_list);
        mysearchviewbar = (SearchView) findViewById(R.id.mysearchview);

        turnoff = (ImageButton)findViewById(R.id.imageButton2);
        //finding listview
        listView = findViewById(R.id.area_listview);
        homebtn = (ImageButton) findViewById(R.id.homebutton);
        final Animation animAlpha2 = AnimationUtils.loadAnimation(this, R.anim.blink);

        homebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                arg1.startAnimation(animAlpha2);
//               homeback();
//        taptoFade();
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        // Block this thread for 2 seconds.
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                        }

                        // After sleep finished blocking, create a Runnable to run on
                        // the UI Thread.
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplication(),AreaListPageMain.class));
                            }

                        });
                    }
                };

                // Don't forget to start the thread.
                thread.start();

            }
       });

        final ArrayAdapter<String> customAdapter = new ArrayAdapter<String>(this, R.layout.activity_custom__layout, R.id.fruits, fruitNames);
        listView.setAdapter(customAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getApplicationContext(),fruitNames[i],Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), listdata.class);
                intent.putExtra("name", fruitNames[i]);
                intent.putExtra("place", place[i]);
                intent.putExtra("image", fruitImages[i]);
                intent.putExtra("phoneNo", phoneNo[i]);
                startActivity(intent);

            }
        });





        mysearchviewbar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                customAdapter.getFilter().filter(s);
                return false;
            }
        });
    }
        public class CustomAdapter extends BaseAdapter {
            @Override
            public int getCount() {
                return fruitImages.length;
            }

            @Override
            public Object getItem(int i) {
                return null;
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View view1 = getLayoutInflater().inflate(R.layout.activity_custom__layout, null);
                //getting view in row_data
                TextView name = view1.findViewById(R.id.fruits);
                ImageView image = view1.findViewById(R.id.images);
                TextView places = view1.findViewById(R.id.placess);


                name.setText(fruitNames[i]);
                image.setImageResource(fruitImages[i]);
                places.setText(place[i]);
                return view1;


            }
        }
    public void clickexit(View view) {

        moveTaskToBack(true);
        Process.killProcess(Process.myPid());
        System.exit(1);
    }

    private void homeback(){
        Intent intent =new Intent(Area_list.this, AreaListPageMain.class);
        startActivity(intent);
    }

    public  void taptoFade(){
        ImageButton imageButton = (ImageButton)findViewById(R.id.homebutton);
        Animation animation = AnimationUtils.loadAnimation(this,R.anim.blink);
        imageButton.startAnimation(animation);
    }
}
