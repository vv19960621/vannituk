package vv.vaishu.vannituk.Area;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import vv.vaishu.vannituk.AreaListPageMain;
import vv.vaishu.vannituk.R;
import vv.vaishu.vannituk.listdata5;
import vv.vaishu.vannituk.listdata6;

public class Area_6 extends AppCompatActivity {

    ListView listView;
    ImageButton homebtn;


    String[] fruitNames6 = {"Rajini","Kamal","Raj","Mohan","karthik","sundhar","muththu","kumar","raju","gowtham","muruga","santhosh"};
    int[] fruitImages6 = {R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro};

    String[] place6 = {"bus_stand","hospital","hospital","oldBus_Stand","postOffice","market","hospital","Bus_stand","postOffice","market","hospital","Bus_stand"};
    String[] phoneNo6 = {"456","0764827382","0778405238","0767513323","456","456","456","456","456","456","456","456"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_6);

        listView = findViewById(R.id.area_listview6);
        Area_6.CustomAdapter customAdapter = new Area_6.CustomAdapter();
        listView.setAdapter(customAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getApplicationContext(),fruitNames[i],Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), listdata6.class);
                intent.putExtra("name6",fruitNames6[i]);
                intent.putExtra("place6",place6[i]);
                intent.putExtra("image6",fruitImages6[i]);
                intent.putExtra("phoneNo6",phoneNo6[i]);
                startActivity(intent);

            }
        });

        homebtn = (ImageButton) findViewById(R.id.homebutton);
        homebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homeback();
            }
        });


    }
    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return fruitImages6.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.activity_custom__layout,null);
            //getting view in row_data
            TextView name = view1.findViewById(R.id.fruits);
            ImageView image = view1.findViewById(R.id.images);
            TextView places = view1.findViewById(R.id.placess);


            name.setText(fruitNames6[i]);
            image.setImageResource(fruitImages6[i]);
            places.setText(place6[i]);
            return view1;



        }
    }

    public void clickexit(View view) {

        moveTaskToBack(true);
        Process.killProcess(Process.myPid());
        System.exit(1);
    }

    private void homeback(){
        Intent intent =new Intent(Area_6.this, AreaListPageMain.class);
        startActivity(intent);
    }
}
