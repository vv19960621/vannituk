package vv.vaishu.vannituk.Area;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import vv.vaishu.vannituk.AreaListPageMain;
import vv.vaishu.vannituk.R;
import vv.vaishu.vannituk.listdata7;
import vv.vaishu.vannituk.listdata8;

public class Area_8 extends AppCompatActivity {

    ListView listView;
    ImageButton homebtn;


    String[] fruitNames8 = {"Rajini","Kamal","Raj","Mohan","karthik","sundhar","muththu","kumar","raju","gowtham","muruga","santhosh"};
    int[] fruitImages8 = {R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro};

    String[] place8 = {"bus_stand","hospital","hospital","oldBus_Stand","postOffice","market","hospital","Bus_stand","postOffice","market","hospital","Bus_stand"};
    String[] phoneNo8 = {"456","0764827382","0778405238","0767513323","456","456","456","456","456","456","456","456"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_8);

        listView = findViewById(R.id.area_listview8);
        Area_8.CustomAdapter customAdapter = new Area_8.CustomAdapter();
        listView.setAdapter(customAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getApplicationContext(),fruitNames[i],Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), listdata8.class);
                intent.putExtra("name8",fruitNames8[i]);
                intent.putExtra("place8",place8[i]);
                intent.putExtra("image8",fruitImages8[i]);
                intent.putExtra("phoneNo8",phoneNo8[i]);
                startActivity(intent);

            }
        });

        homebtn = (ImageButton) findViewById(R.id.homebutton);
        homebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homeback();
            }
        });


    }
    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return fruitImages8.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.activity_custom__layout,null);
            //getting view in row_data
            TextView name = view1.findViewById(R.id.fruits);
            ImageView image = view1.findViewById(R.id.images);
            TextView places = view1.findViewById(R.id.placess);


            name.setText(fruitNames8[i]);
            image.setImageResource(fruitImages8[i]);
            places.setText(place8[i]);
            return view1;



        }
    }
    public void clickexit(View view) {

        moveTaskToBack(true);
        Process.killProcess(Process.myPid());
        System.exit(1);
    }

    private void homeback(){
        Intent intent =new Intent(Area_8.this, AreaListPageMain.class);
        startActivity(intent);
    }
}
