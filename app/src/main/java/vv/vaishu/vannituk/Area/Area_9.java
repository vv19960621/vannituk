package vv.vaishu.vannituk.Area;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import vv.vaishu.vannituk.AreaListPageMain;
import vv.vaishu.vannituk.R;
import vv.vaishu.vannituk.listdata8;
import vv.vaishu.vannituk.listdata9;

public class Area_9 extends AppCompatActivity {

    ListView listView;
    ImageButton homebtn;


    String[] fruitNames9 = {"Rajini","Kamal","Raj","Mohan","karthik","sundhar","muththu","kumar","raju","gowtham","muruga","santhosh"};
    int[] fruitImages9 = {R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro,R.drawable.pro};

    String[] place9 = {"bus_stand","hospital","hospital","oldBus_Stand","postOffice","market","hospital","Bus_stand","postOffice","market","hospital","Bus_stand"};
    String[] phoneNo9 = {"456","0764827382","0778405238","0767513323","456","456","456","456","456","456","456","456"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_9);

        listView = findViewById(R.id.area_listview9);
        Area_9.CustomAdapter customAdapter = new Area_9.CustomAdapter();
        listView.setAdapter(customAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getApplicationContext(),fruitNames[i],Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), listdata9.class);
                intent.putExtra("name9",fruitNames9[i]);
                intent.putExtra("place9",place9[i]);
                intent.putExtra("image9",fruitImages9[i]);
                intent.putExtra("phoneNo9",phoneNo9[i]);
                startActivity(intent);

            }
        });

        homebtn = (ImageButton) findViewById(R.id.homebutton);
        homebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homeback();
            }
        });


    }
    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return fruitImages9.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.activity_custom__layout,null);
            //getting view in row_data
            TextView name = view1.findViewById(R.id.fruits);
            ImageView image = view1.findViewById(R.id.images);
            TextView places = view1.findViewById(R.id.placess);


            name.setText(fruitNames9[i]);
            image.setImageResource(fruitImages9[i]);
            places.setText(place9[i]);
            return view1;



        }
    }

    public void clickexit(View view) {

        moveTaskToBack(true);
        Process.killProcess(Process.myPid());
        System.exit(1);
    }

    private void homeback(){
        Intent intent =new Intent(Area_9.this, AreaListPageMain.class);
        startActivity(intent);
    }
}
