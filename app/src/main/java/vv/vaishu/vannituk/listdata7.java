package vv.vaishu.vannituk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import vv.vaishu.vannituk.Area.Area_6;
import vv.vaishu.vannituk.Area.Area_7;

public class listdata7 extends AppCompatActivity {

    private static final int REQUEST_CALL = 1;
    TextView listdata;
    ImageView imageView;
    private  TextView phoneNumber;
    ImageButton backbtn7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdata7);
        listdata = findViewById(R.id.listdata7);
        imageView = findViewById(R.id.imageView7);
        phoneNumber = findViewById(R.id.edit_text_numbers7);
        ImageView imageCall = findViewById(R.id.image_calls7);
        backbtn7 = findViewById(R.id.back7);

        Intent intent = getIntent();
        String receivedName7 =  intent.getStringExtra("name7");
        int receivedImage7 = intent.getIntExtra("image7",0);
        String recievedNumber7 = intent.getStringExtra("phoneNo7");
        listdata.setText(receivedName7);
        imageView.setImageResource(receivedImage7);
        phoneNumber.setText(recievedNumber7);

        //enable back Button
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCalls();
            }
        });
        backbtn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back7();
            }
        });

    }
    private  void back7(){
        Intent intent =new Intent(listdata7.this, Area_7.class);
        startActivity(intent);
    }


    private void makePhoneCalls() {
        String number = phoneNumber.getText().toString();
        if (number.trim().length() > 0) {

            if (ContextCompat.checkSelfPermission(listdata7.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(listdata7.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        } else {
            Toast.makeText(listdata7.this, "Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
