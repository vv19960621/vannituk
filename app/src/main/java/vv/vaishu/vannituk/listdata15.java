package vv.vaishu.vannituk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import vv.vaishu.vannituk.Area.Area_14;
import vv.vaishu.vannituk.Area.Area_15;

public class listdata15 extends AppCompatActivity {
    private static final int REQUEST_CALL = 1;
    TextView listdata;
    ImageView imageView;
    private  TextView phoneNumber;
    ImageButton backbtn14;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdata15);
        listdata = findViewById(R.id.listdata10);
        imageView = findViewById(R.id.imageView10);
        phoneNumber = findViewById(R.id.edit_text_numbers10);
        ImageView imageCall = findViewById(R.id.image_calls10);
        backbtn14 = findViewById(R.id.back10);

        Intent intent = getIntent();
        String receivedName10 =  intent.getStringExtra("name15");
        int receivedImage10 = intent.getIntExtra("image15",0);
        String recievedNumber10 = intent.getStringExtra("phoneNo15");
        listdata.setText(receivedName10);
        imageView.setImageResource(receivedImage10);
        phoneNumber.setText(recievedNumber10);

        //enable back Button
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCalls();
            }
        });
        backbtn14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back14();
            }
        });

    }
    private  void back14(){
        Intent intent =new Intent(listdata15.this, Area_15.class);
        startActivity(intent);
    }


    private void makePhoneCalls() {
        String number = phoneNumber.getText().toString();
        if (number.trim().length() > 0) {

            if (ContextCompat.checkSelfPermission(listdata15.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(listdata15.this,
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
            } else {
                String dial = "tel:" + number;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        } else {
            Toast.makeText(listdata15.this, "Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
